require "administrate/base_dashboard"

class RollerDashboard < Administrate::BaseDashboard
 
  ATTRIBUTE_TYPES = {
    id: Field::Number,
    date: Field::String,
    verification: Field::String,
    registered: Field::String,
    membership_no: Field::String,
    name: Field::String,
    id_no: Field::String,
    phone_no: Field::String,
    mpesa: Field::String,
    location: Field::String,
    sales_rep: Field::String,
    region: Field::String,
    created_at: Field::DateTime,
    updated_at: Field::DateTime,
  }.freeze

   COLLECTION_ATTRIBUTES = [
    :name,
    :membership_no,
    :phone_no,
    :mpesa,
  ].freeze

  SHOW_PAGE_ATTRIBUTES = [
    :id,
    :date,
    :verification,
    :registered,
    :membership_no,
    :name,
    :id_no,
    :phone_no,
    :mpesa,
    :location,
    :sales_rep,
    :region,
    :created_at,
    :updated_at,
  ].freeze

  FORM_ATTRIBUTES = [
    :date,
    :verification,
    :registered,
    :membership_no,
    :name,
    :id_no,
    :phone_no,
    :mpesa,
    :location,
    :sales_rep,
    :region,
  ].freeze
end
