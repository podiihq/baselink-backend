require "administrate/base_dashboard"

class StudentDashboard < Administrate::BaseDashboard
 
  ATTRIBUTE_TYPES = {
    id: Field::Number,
    date: Field::String,
    verification: Field::String,
    registered: Field::String,
    membership_no: Field::String,
    name: Field::String,
    id_no: Field::String,
    phone_no: Field::String,
    institute: Field::String,
    nita: Field::String,
    on_attachment: Field::String,
    attachment_period: Field::String,
    plant_operator: Field::String,
    location: Field::String,
    mpesa_code: Field::String,
    created_at: Field::DateTime,
    updated_at: Field::DateTime,
  }.freeze

  COLLECTION_ATTRIBUTES = [
    :name,
    :phone_no,
    :location,
    :attachment_period,
    :mpesa_code,
  ].freeze

  SHOW_PAGE_ATTRIBUTES = [
    :id,
    :date,
    :verification,
    :registered,
    :membership_no,
    :name,
    :id_no,
    :phone_no,
    :institute,
    :nita,
    :on_attachment,
    :attachment_period,
    :plant_operator,
    :location,
    :mpesa_code,
    :created_at,
    :updated_at,
  ].freeze

  FORM_ATTRIBUTES = [
    :date,
    :verification,
    :registered,
    :membership_no,
    :name,
    :id_no,
    :phone_no,
    :institute,
    :nita,
    :on_attachment,
    :attachment_period,
    :plant_operator,
    :location,
    :mpesa_code,
  ].freeze
end
