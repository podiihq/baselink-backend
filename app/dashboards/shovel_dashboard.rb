require "administrate/base_dashboard"

class ShovelDashboard < Administrate::BaseDashboard
  # ATTRIBUTE_TYPES
  # a hash that describes the type of each of the model's fields.
  #
  # Each different type represents an Administrate::Field object,
  # which determines how the attribute is displayed
  # on pages throughout the dashboard.
  ATTRIBUTE_TYPES = {
    id: Field::Number,
    date: Field::String,
    verification: Field::String,
    registered: Field::String,
    membership_no: Field::String,
    name: Field::String,
    id_no: Field::String,
    phone_no: Field::String,
    mpesa: Field::String,
    location: Field::String,
    sales_rep: Field::String,
    region: Field::String,
    created_at: Field::DateTime,
    updated_at: Field::DateTime,
  }.freeze

  # COLLECTION_ATTRIBUTES
  # an array of attributes that will be displayed on the model's index page.
  #
  # By default, it's limited to four items to reduce clutter on index pages.
  # Feel free to add, remove, or rearrange items.
  COLLECTION_ATTRIBUTES = [
    :id,
    :name,
    :id_no,
    :phone_no,
    :verification
  ].freeze

  # SHOW_PAGE_ATTRIBUTES
  # an array of attributes that will be displayed on the model's show page.
  SHOW_PAGE_ATTRIBUTES = [
    :id,
    :date,
    :verification,
    :registered,
    :membership_no,
    :name,
    :id_no,
    :phone_no,
    :mpesa,
    :location,
    :sales_rep,
    :region,
    :created_at,
    :updated_at,
  ].freeze

  # FORM_ATTRIBUTES
  # an array of attributes that will be displayed
  # on the model's form (`new` and `edit`) pages.
  FORM_ATTRIBUTES = [
    :date,
    :verification,
    :registered,
    :membership_no,
    :name,
    :id_no,
    :phone_no,
    :mpesa,
    :location,
    :sales_rep,
    :region,
  ].freeze

  # Overwrite this method to customize how shovels are displayed
  # across all pages of the admin dashboard.
  #
  # def display_resource(shovel)
  #   "Shovel ##{shovel.id}"
  # end
end
