require "administrate/base_dashboard"

class TemplateDashboard < Administrate::BaseDashboard
  
  ATTRIBUTE_TYPES = {
    id: Field::Number,
    date: Field::String,
    name: Field::String,
    gender: Field::String,
    qualification: Field::String,
    resume: Field::String,
    id_no: Field::String,
    nssf: Field::String,
    nhif: Field::String,
    pin_no: Field::String,
    background_check: Field::String,
    verification_of_documents: Field::String,
    contacts: Field::String,
    created_at: Field::DateTime,
    updated_at: Field::DateTime,
  }.freeze

  COLLECTION_ATTRIBUTES = [
    :name,
    :gender,
    :id_no,
    :pin_no,
  ].freeze

  SHOW_PAGE_ATTRIBUTES = [
    :id,
    :date,
    :name,
    :gender,
    :qualification,
    :resume,
    :id_no,
    :nssf,
    :nhif,
    :pin_no,
    :background_check,
    :verification_of_documents,
    :contacts,
    :created_at,
    :updated_at,
  ].freeze

  FORM_ATTRIBUTES = [
    :date,
    :name,
    :gender,
    :qualification,
    :resume,
    :id_no,
    :nssf,
    :nhif,
    :pin_no,
    :background_check,
    :verification_of_documents,
    :contacts,
  ].freeze
end
