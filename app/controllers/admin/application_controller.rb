module Admin
  class ApplicationController < Administrate::ApplicationController
    before_action :authenticate_user!
    protect_from_forgery with: :exception
  end
end
