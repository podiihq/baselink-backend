require 'csv'

User.create!(email: 'admin@admin.com', password: 'admin1234')
User.create!(email: 'getty@getty.com', password: 'getty1234')

CSV.foreach("./lib/seeds/bulldozer.csv") do |row|
  print '.'
  Bulldozer.new(date: row[0], 
                verification: row[1], 
                registered: row[2],
                membership_no: row[3],
                name: row[4],
                id_no: row[5],
                phone_no: row[6],
                mpesa: row[7],
                location: row[8],
                sales_rep: row[9],
                region: row[10]
               ).save!
end

CSV.foreach("./lib/seeds/excavators.csv") do |row|
  print '.'
  Excavator.new(date: row[0], 
                verification: row[1], 
                registered: row[2],
                reg_no: row[3],
                name: row[4],
                id_no: row[5],
                phone_no: row[6],
                mpesa: row[7],
                location: row[8],
                sales_rep: row[9],
                region: row[10]
               ).save!
end
CSV.foreach("./lib/seeds/drivers.csv") do |row|
  print '.'
  Driver.new(date: row[0], 
                verification: row[1], 
                registered: row[2],
                membership_no: row[3],
                name: row[4],
                artic: row[5],
                id_no: row[6],
                phone_no: row[7],
                mpesa: row[8],
                location: row[9],
                sales_rep: row[10],
                region: row[11],
                engaged: row[12]
                ).save!
end

CSV.foreach("./lib/seeds/generaloperators.csv") do |row|
  print '.'
  GeneralOperator.new(date: row[0], 
                verified: row[1], 
                registered: row[2],
                reg_no: row[3],
                name: row[4],
                id_no: row[5],
                phone_no: row[6],
                escavator: row[7],
                grader: row[8],
                backhoe: row[9],
                shovel: row[10],
                forklift: row[11],
                frontloader: row[12],
                roller: row[13],
                bulldozer: row[14],
                drivers: row[15],
                crane: row[16],
                plant_mechanic: row[17],
                engaged: row[18],
                mpesa: row[19],
                location: row[20],
                sales_rep: row[21],
                region: row[22]
                ).save!
end

CSV.foreach("./lib/seeds/students.csv") do |row|
  print '.'
  Student.new(date: row[0], 
              verification: row[1], 
              registered: row[2],
              membership_no: row[3],
              name: row[4],
              id_no: row[5],
              phone_no: row[6],
              institute: row[7],
              nita: row[8],
              on_attachment: row[9],
              attachment_period: row[10],
              plant_operator: row[11],
              location: row[12],
              mpesa_code: row[13]
              ).save!
end

CSV.foreach("./lib/seeds/crawler tractor.csv") do |row|
  print '.'
  CrawlerTractor.new(date: row[0], 
                     verification: row[1], 
                     registered: row[2],
                     membership_no: row[3],
                     name: row[4],
                     id_no: row[5],
                     phone_no: row[6],
                     mpesa: row[7],
                     location: row[8],
                     sales_rep: row[9],
                     region: row[10]
                     ).save!
end

CSV.foreach("./lib/seeds/backhoe.csv") do |row|
  print '.'
  Backho.new(date: row[0], 
                verification: row[1], 
                registered: row[2],
                membership_no: row[3],
                name: row[4],
                id_no: row[5],
                phone_no: row[6],
                mpesa: row[7],
                location: row[8],
                sales_rep: row[9],
                region: row[10]
               ).save!
end

CSV.foreach("./lib/seeds/forklift.csv") do |row|
  print '.'
  ForkLift.new(date: row[0], 
                     verification: row[1], 
                     registered: row[2],
                     membership_no: row[3],
                     name: row[4],
                     id_no: row[5],
                     phone_no: row[6],
                     mpesa: row[7],
                     location: row[8],
                     sales_rep: row[9],
                     region: row[10]
                     ).save!
end

 CSV.foreach("./lib/seeds/wheel loader.csv") do |row|
  print '.'
  WheelLoader.new(date: row[0], 
                     verification: row[1], 
                     registered: row[2],
                     membership_no: row[3],
                     name: row[4],
                     id_no: row[5],
                     phone_no: row[6],
                     mpesa: row[7],
                     location: row[8],
                     sales_rep: row[9],
                     region: row[10]
                     ).save!
end

CSV.foreach("./lib/seeds/roller.csv") do |row|
  print '.'
  Roller.new(date: row[0], 
                     verification: row[1], 
                     registered: row[2],
                     membership_no: row[3],
                     name: row[4],
                     id_no: row[5],
                     phone_no: row[6],
                     mpesa: row[7],
                     location: row[8],
                     sales_rep: row[9],
                     region: row[10]
                     ).save!
end

CSV.foreach("./lib/seeds/crane operators.csv") do |row|
  print '.'
  CraneOperator.new(date: row[0], 
                     verification: row[1], 
                     registered: row[2],
                     membership_no: row[3],
                     name: row[4],
                     id_no: row[5],
                     phone_no: row[6],
                     mpesa: row[7],
                     location: row[8],
                     sales_rep: row[9],
                     region: row[10]
                     ).save!
end

CSV.foreach("./lib/seeds/template.csv") do |row|
  print '.'
  Template.new(date: row[0], 
              name: row[1], 
              gender: row[2],
               qualification: row[3],
               resume: row[4],
              id_no: row[5],
              nssf: row[6],
               nhif: row[7],
               pin_no: row[8],
               background_check: row[9],
               verification_of_documents: row[10],
               contacts: row[10]
              ).save!
end

CSV.foreach("./lib/seeds/mechanics.csv") do |row|
  print '.'
  Mechanic.new(date: row[0], 
               verification: row[1], 
               registered: row[2],
               membership_no: row[3],
               name: row[4],
               id_no: row[5],
               phone_no: row[6],
               mpesa: row[7],
               location: row[8],
               sales_rep: row[9],
               region: row[10]
              ).save!
end

CSV.foreach("./lib/seeds/shovels.csv") do |row|
  print '.'
  Shovel.new(date: row[0], 
               verification: row[1], 
               registered: row[2],
               membership_no: row[3],
               name: row[4],
               id_no: row[5],
               phone_no: row[6],
               mpesa: row[7],
               location: row[8],
               sales_rep: row[9],
               region: row[10]
              ).save!
end
CSV.foreach("./lib/seeds/frontloader.csv") do |row|
  print '.'
  FrontLoader.new(date: row[0], 
               verification: row[1], 
               registered: row[2],
               membership_no: row[3],
               name: row[4],
               id_no: row[5],
               phone_no: row[6],
               mpesa: row[7],
               location: row[8],
               sales_rep: row[9],
               region: row[10]
              ).save!
end

CSV.foreach("./lib/seeds/sitemanagers.csv") do |row|
  print '.'
  SiteManager.new(date: row[0], 
               verification: row[1], 
               registered: row[2],
               membership_no: row[3],
               name: row[4],
               id_no: row[5],
               phone_no: row[6],
               mpesa: row[7],
               location: row[8],
               sales_rep: row[9],
               region: row[10]
              ).save!
end

CSV.foreach("./lib/seeds/motorgrader.csv") do |row|
  print '.'
  MotorGrader.new(date: row[0], 
               verification: row[1], 
               registered: row[2],
               membership_no: row[3],
               name: row[4],
               id_no: row[5],
               phone_no: row[6],
               mpesa: row[7],
               location: row[8],
               sales_rep: row[9],
               region: row[10]
              ).save!
end
