class CreateBackhos < ActiveRecord::Migration[5.2]
  def change
    create_table :backhos do |t|
      t.string :date
      t.string :verification
      t.string :registered
      t.string :membership_no
      t.string :name
      t.string :id_no
      t.string :phone_no
      t.string :mpesa
      t.string :location
      t.string :sales_rep
      t.string :region

      t.timestamps
    end
  end
end
