class CreateTemplates < ActiveRecord::Migration[5.2]
  def change
    create_table :templates do |t|
      t.string :date
      t.string :name
      t.string :gender
      t.string :qualification
      t.string :resume
      t.string :id_no
      t.string :nssf
      t.string :nhif
      t.string :pin_no
      t.string :background_check
      t.string :verification_of_documents
      t.string :contacts

      t.timestamps
    end
  end
end
