class RenameStudentColumns < ActiveRecord::Migration[5.2]
  def change
    rename_column(:students, :mpesa, :mpesa_code)
    remove_column(:students, :excavator, :string)
    remove_column(:students, :grader, :string)
    remove_column(:students, :backhoe, :string)
    remove_column(:students, :wheel_shovel, :string)
    remove_column(:students, :forklift, :string)
    remove_column(:students, :front_loader, :string)
    remove_column(:students, :roller, :string)
    remove_column(:students, :bulldozer, :string)
    remove_column(:students, :drivers, :string)
    remove_column(:students, :civil_engineers, :string)
    remove_column(:students, :plant_mechanic, :string)
    remove_column(:students, :engaged, :string)
    remove_column(:students, :sales_rep, :string)
    remove_column(:students, :region, :string)
    add_column(:students, :institute, :string)
    add_column(:students, :nita, :string)
    add_column(:students, :on_attachment, :string)
    add_column(:students, :attachment_period, :string)
    add_column(:students, :plant_operator, :string)
  end
end
