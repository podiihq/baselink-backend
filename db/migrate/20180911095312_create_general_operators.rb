class CreateGeneralOperators < ActiveRecord::Migration[5.2]
  def change
    create_table :general_operators do |t|
      t.string :date
      t.string :verified
      t.string :registered
      t.string :reg_no
      t.string :name
      t.string :id_no
      t.string :phone_no
      t.string :escavator
      t.string :grader
      t.string :backhoe
      t.string :shovel
      t.string :forklift
      t.string :frontloader
      t.string :roller
      t.string :bulldozer
      t.string :drivers
      t.string :crane
      t.string :plant_mechanic
      t.string :engaged
      t.string :mpesa
      t.string :location
      t.string :sales_rep
      t.string :region

      t.timestamps
    end
  end
end
