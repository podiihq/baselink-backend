class CreateStudents < ActiveRecord::Migration[5.2]
  def change
    create_table :students do |t|
      t.string :date
      t.string :verification
      t.string :registered
      t.string :membership_no
      t.string :name
      t.string :id_no
      t.string :phone_no
      t.string :excavator
      t.string :grader
      t.string :backhoe
      t.string :wheel_shovel
      t.string :forklift
      t.string :front_loader
      t.string :roller
      t.string :bulldozer
      t.string :drivers
      t.string :civil_engineers
      t.string :plant_mechanic
      t.string :engaged
      t.string :mpesa
      t.string :location
      t.string :sales_rep
      t.string :region

      t.timestamps
    end
  end
end
