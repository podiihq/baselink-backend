# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2018_10_24_153842) do

  create_table "backhos", force: :cascade do |t|
    t.string "date"
    t.string "verification"
    t.string "registered"
    t.string "membership_no"
    t.string "name"
    t.string "id_no"
    t.string "phone_no"
    t.string "mpesa"
    t.string "location"
    t.string "sales_rep"
    t.string "region"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "bulldozers", force: :cascade do |t|
    t.string "date"
    t.string "verification"
    t.string "registered"
    t.string "membership_no"
    t.string "name"
    t.string "id_no"
    t.string "phone_no"
    t.string "mpesa"
    t.string "location"
    t.string "sales_rep"
    t.string "region"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "crane_operators", force: :cascade do |t|
    t.string "date"
    t.string "verification"
    t.string "registered"
    t.string "membership_no"
    t.string "name"
    t.string "id_no"
    t.string "phone_no"
    t.string "mpesa"
    t.string "location"
    t.string "sales_rep"
    t.string "region"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "crawler_tractors", force: :cascade do |t|
    t.string "date"
    t.string "verification"
    t.string "registered"
    t.string "membership_no"
    t.string "name"
    t.string "id_no"
    t.string "phone_no"
    t.string "mpesa"
    t.string "location"
    t.string "sales_rep"
    t.string "region"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "drivers", force: :cascade do |t|
    t.string "date"
    t.string "verification"
    t.string "registered"
    t.string "membership_no"
    t.string "name"
    t.string "artic"
    t.string "id_no"
    t.string "phone_no"
    t.string "mpesa"
    t.string "location"
    t.string "sales_rep"
    t.string "region"
    t.string "engaged"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "excavators", force: :cascade do |t|
    t.string "date"
    t.string "verification"
    t.string "registered"
    t.string "reg_no"
    t.string "name"
    t.string "id_no"
    t.string "phone_no"
    t.string "mpesa"
    t.string "location"
    t.string "sales_rep"
    t.string "region"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "fork_lifts", force: :cascade do |t|
    t.string "date"
    t.string "verification"
    t.string "registered"
    t.string "membership_no"
    t.string "name"
    t.string "id_no"
    t.string "phone_no"
    t.string "mpesa"
    t.string "location"
    t.string "sales_rep"
    t.string "region"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "front_loaders", force: :cascade do |t|
    t.string "date"
    t.string "verification"
    t.string "registered"
    t.string "membership_no"
    t.string "name"
    t.string "id_no"
    t.string "phone_no"
    t.string "mpesa"
    t.string "location"
    t.string "sales_rep"
    t.string "region"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "general_operators", force: :cascade do |t|
    t.string "date"
    t.string "verified"
    t.string "registered"
    t.string "reg_no"
    t.string "name"
    t.string "id_no"
    t.string "phone_no"
    t.string "escavator"
    t.string "grader"
    t.string "backhoe"
    t.string "shovel"
    t.string "forklift"
    t.string "frontloader"
    t.string "roller"
    t.string "bulldozer"
    t.string "drivers"
    t.string "crane"
    t.string "plant_mechanic"
    t.string "engaged"
    t.string "mpesa"
    t.string "location"
    t.string "sales_rep"
    t.string "region"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "mechanics", force: :cascade do |t|
    t.string "date"
    t.string "verification"
    t.string "registered"
    t.string "membership_no"
    t.string "name"
    t.string "id_no"
    t.string "phone_no"
    t.string "mpesa"
    t.string "location"
    t.string "sales_rep"
    t.string "region"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "motor_graders", force: :cascade do |t|
    t.string "date"
    t.string "verification"
    t.string "registered"
    t.string "membership_no"
    t.string "name"
    t.string "id_no"
    t.string "phone_no"
    t.string "mpesa"
    t.string "location"
    t.string "sales_rep"
    t.string "region"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "rollers", force: :cascade do |t|
    t.string "date"
    t.string "verification"
    t.string "registered"
    t.string "membership_no"
    t.string "name"
    t.string "id_no"
    t.string "phone_no"
    t.string "mpesa"
    t.string "location"
    t.string "sales_rep"
    t.string "region"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "shovels", force: :cascade do |t|
    t.string "date"
    t.string "verification"
    t.string "registered"
    t.string "membership_no"
    t.string "name"
    t.string "id_no"
    t.string "phone_no"
    t.string "mpesa"
    t.string "location"
    t.string "sales_rep"
    t.string "region"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "site_managers", force: :cascade do |t|
    t.string "date"
    t.string "verification"
    t.string "registered"
    t.string "membership_no"
    t.string "name"
    t.string "id_no"
    t.string "phone_no"
    t.string "mpesa"
    t.string "location"
    t.string "sales_rep"
    t.string "region"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "students", force: :cascade do |t|
    t.string "date"
    t.string "verification"
    t.string "registered"
    t.string "membership_no"
    t.string "name"
    t.string "id_no"
    t.string "phone_no"
    t.string "mpesa_code"
    t.string "location"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "institute"
    t.string "nita"
    t.string "on_attachment"
    t.string "attachment_period"
    t.string "plant_operator"
  end

  create_table "templates", force: :cascade do |t|
    t.string "date"
    t.string "name"
    t.string "gender"
    t.string "qualification"
    t.string "resume"
    t.string "id_no"
    t.string "nssf"
    t.string "nhif"
    t.string "pin_no"
    t.string "background_check"
    t.string "verification_of_documents"
    t.string "contacts"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  create_table "wheel_loaders", force: :cascade do |t|
    t.string "date"
    t.string "verification"
    t.string "registered"
    t.string "membership_no"
    t.string "name"
    t.string "id_no"
    t.string "phone_no"
    t.string "mpesa"
    t.string "location"
    t.string "sales_rep"
    t.string "region"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

end
