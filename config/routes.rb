Rails.application.routes.draw do
  devise_for :users
  root to: "admin/bulldozers#index"
  namespace :admin do
      resources :bulldozers
      resources :excavators
      resources :drivers
      resources :general_operators
      resources :students
      resources :crawler_tractors
      resources :backhos
      resources :fork_lifts
      resources :wheel_loaders
      resources :rollers
      resources :crane_operators
      resources :templates
      resources :mechanics
      resources :shovels
      resources :site_managers
      resources :front_loaders
      resources :motor_graders

      root to: "bulldozers#index"
    end
end
